# yolo

This is an experimental trying to create editor in rust that uses :

- [gluon](https://github.com/gluon-lang/gluon) as plugin system

Most of the initial GUI are being implemented in regards to emacs minimalist gui.

TODO :

- [ ] It should be scriptable & extensible as emacs has
- [ ] It should supports client & server mode as emacs has
- [ ] It should be fast :))
- [ ] It should support modern GUI
- [ ] It should support LSP & DAP protocols
- [ ] It should support something like company-globals (auto-complete based on last abbrev typed)
- [ ] It should have better font-face declaration (declarative or scripted)
- [ ] It should be multiplatform
- [ ] Installing should be easy as `cargo install yolo-mui` or `cargo install yolo-server`
- [ ] Installing plugin should be straighforward
- [ ] May support auto reloads script ??
- [ ] Support for exposing internal rendering into scriptable layers
- [ ] Should be secure for plugin (what module to include, queryable, descriptable)
- [ ] Expose common folder structures & heap (arena) over plugins (using shm)
- [ ] By default use ripgrep indexes :))
- [ ] projectile like project exploration
- [ ] git lens capable plugins
- [ ] forge like capable plugins
- [ ] restclient like capable plugins

# Plans

- the only one that can writes into disk are only server process
- client (including plugins) are shares `shm` pages from server
- client plugins only able to communicate with server or do something with client ui
- server plugins only able to communicate with server administratively by using stdout then
  if necessary any non admin operations can be exposed in any transports.
- server plugins & client could interact directly with shm, while client plugins can't
- client could persist caches buffer from shm as please as they want
- most client actions are being done in async flow (except code edit/changes)

# Milestones

## Buffer implementation

create buffer implementation (need to look for atom implementation buffer & vscode implementation though
emacs buffer implementation will be the simplest one)

- vscode are using PieceTable implementation (https://en.wikipedia.org/wiki/Piece_table)
- atom sometimes want to outsource their textbuffer implementation in rust (https://github.com/atom-archive/xray)
- xi-editor got some discussions regarding their CRDT data structure (https://github.com/xi-editor/xi-editor/issues/1187)
- gap buffer emacs implementation could be straighforward as ever

maybe we could implement some text buffer adapter ? (we could do some static dispatch in some of the function
by modelling the trait in associactive type construction or as composable structs)

Notes:
- we don't need parallel edits, edits could be queued
- we don't need editor that can do simultenaous edits with different sessions
- we need fast implementation for syntax highlighting, edits, clear
  (it should handle most of the problem in at least O(n log N))
- the buffer should be paged into shm nicely (mmap)

## Core Editor implementation

We
